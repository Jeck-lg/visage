#Test project for Cleevio

##Build
Standard build via Maven

##Database
Application uses H2 embedded database

##Plugins/dependencies
Lombok\
Mapstruct

###Actual state of project
Working create functional for Watch Entity\
Integration test of controller\
Unit tests for mapper and service

