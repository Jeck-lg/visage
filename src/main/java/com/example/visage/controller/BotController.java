package com.example.visage.controller;

import com.example.visage.dto.AvailableTimesDTO;
import com.example.visage.mapper.ReservationMapper;
import com.example.visage.service.ReservationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.sql.Date;

@RestController
@AllArgsConstructor
public class BotController {

    @Autowired
    ReservationService reservationService;

    @Autowired
    ReservationMapper reservationMapper;


    @ExceptionHandler({ MethodArgumentNotValidException.class, DataIntegrityViolationException.class, HttpClientErrorException.Forbidden.class})
    public ResponseEntity<Void> handleException() {

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/api/{service}/{date}/{time}/{info}")
    public ResponseEntity<Void> createReservation(
            @RequestHeader("api-key") String key,
            @PathVariable String service,
            @PathVariable String date,
            @PathVariable String time,
            @PathVariable String info) {

        if (key.equals("b115d49b-6272-4505-a4e3-cd2a69cc4969")){
            reservationService.createReservation(reservationMapper.mapToDTO(service, date, time, info));

            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/api/{date}")
    public AvailableTimesDTO getAvailableTimes(
            @RequestHeader("api-key") String key, @PathVariable String date) {

        if (key.equals("b115d49b-6272-4505-a4e3-cd2a69cc4969")){
            return reservationService.getAvailableTimes(Date.valueOf(date).toLocalDate());
        } else {
            return new AvailableTimesDTO();
        }
    }

}
