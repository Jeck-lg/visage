package com.example.visage.service;

import com.example.visage.dto.AvailableTimesDTO;
import com.example.visage.dto.ReservationDTO;
import com.example.visage.entity.ReservationEntity;
import com.example.visage.mapper.ReservationMapper;
import com.example.visage.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;

@Service
public class ReservationService {

	@Autowired
	ReservationRepository reservationRepository;

	@Autowired
	ReservationMapper reservationMapper;


	public void createReservation(ReservationDTO reservationDTO){
		reservationRepository.save(reservationMapper.mapToReservationEntity(reservationDTO));
	}

	public AvailableTimesDTO getAvailableTimes(LocalDate date){
		return reservationMapper.mapToAvailableTimes(reservationRepository.findAllByDate(date));
	}

}
