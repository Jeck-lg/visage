package com.example.visage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReservationDTO {

	@NotNull
	private String service;

	@NotNull
	private String info;

	@NotNull
	private String date;

	@NotNull
	private String time;

}
