package com.example.visage.repository;

import com.example.visage.entity.ReservationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
@Transactional
public interface ReservationRepository extends JpaRepository<ReservationEntity, Long> {

	public List<ReservationEntity> findAllByDate(LocalDate date);

}
