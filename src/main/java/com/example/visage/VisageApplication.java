package com.example.visage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisageApplication {

	public static void main(String[] args) {
		SpringApplication.run(VisageApplication.class, args);
	}

}
