package com.example.visage.mapper;

import com.example.visage.dto.AvailableTimesDTO;
import com.example.visage.dto.ReservationDTO;
import com.example.visage.entity.ReservationEntity;
import com.example.visage.enums.ServiceType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Component
public class ReservationMapper {

	public ReservationDTO mapToDTO(String service, String date, String time, String info){

		ReservationDTO dto = new ReservationDTO();

		dto.setService(service);
		dto.setDate(date);
		dto.setTime(time);
		dto.setInfo(info);

		return dto;
	}

	public ReservationEntity mapToReservationEntity(ReservationDTO dto){

		ReservationEntity reservationEntity = new ReservationEntity();

		reservationEntity.setService(ServiceType.valueOf(dto.getService().toUpperCase()));
		reservationEntity.setDate(Date.valueOf(dto.getDate()).toLocalDate());
		reservationEntity.setTime(Time.valueOf(dto.getTime() + ":00").toLocalTime());
		reservationEntity.setInfo(dto.getInfo());

		return reservationEntity;
	}

	public AvailableTimesDTO mapToAvailableTimes(List<ReservationEntity> reservationEntities){

		AvailableTimesDTO dto = setup();

		if (reservationEntities.isEmpty()){
			return dto;
		}

		for (ReservationEntity reservationEntity : reservationEntities){

			dto.getTimeList().remove(reservationEntity.getTime());

			if (reservationEntity.getService().equals(ServiceType.SHOOTING)|| reservationEntity.getService().equals(ServiceType.WEDDING)){
				dto.getTimeList().remove(reservationEntity.getTime().plusHours(1));
			}

		}

		return dto;
	}

	private AvailableTimesDTO setup(){
		AvailableTimesDTO dto = new AvailableTimesDTO();

		List<LocalTime> timeList = new ArrayList<>();

		for( int i = 9; i <= 17 ;i++ ){
			timeList.add(LocalTime.of(i,0));
		}

		dto.setTimeList(timeList);

		return dto;
	}

}
